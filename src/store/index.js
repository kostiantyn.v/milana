import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isHiddenChat: true,
  
  },
  mutations: {
    showChat(state, value) {
      state.isHiddenChat = value;
    }
  },
  actions: {
  },
})
