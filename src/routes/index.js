import Vue from 'vue'
import VueRouter from 'vue-router'
import authRoutes from './groups/auth-routes'
import global from './groups/global'
Vue.use(VueRouter)

// const routes = [
//   {
//     component: () => import('../views/pages/home'),
//     path: '/',
//     name: 'home'
//   },
//   {
//     component: () => import('../views/pages/about'),
//     path: '/about',
//     name: 'about',
//   },
//   {
//     component: () => import('../views/pages/profiles'),
//     path: '/profile',
//     name: 'profile',
//   }
// ]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...authRoutes, ...global]
})

export default router
