export default [
    {
        component: () => import('../../layouts/global'),
        path: '/',
        redirect: { name: 'home' },
        children: [
            {
                component: () => import('../../views/pages/profiles'),
                path: '/profile',
                name: 'profile'
            },
            {
                component: () => import('../../views/pages/membership'),
                path: '/membership',
                name: 'membership'
            },
            {
                component: () => import('../../views/pages/catalog'),
                path: '/catalog',
                name: 'catalog'
            }
        ]

    }
]