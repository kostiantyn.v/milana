export default [
    {
        component: () => import('../../layouts/auth'),
        path: '/',
        redirect: { name: 'home' },
        children: [
            {
                component: () => import('../../views/pages/home'),
                path: '/',
                name: 'home'
            }
        ]
    }
]